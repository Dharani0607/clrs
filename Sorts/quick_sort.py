def partition(arr, start, end):
    pivot = arr[end]
    i = start - 1
    for j in range(start, end):
        if arr[j] <= pivot:
            i += 1
            arr[i], arr[j] = arr[j], arr[i]
    arr[i+1], arr[end] = arr[end], arr[i+1]
    return i+1

def quick_sort(arr, start, end):
    if start<end:
        pivotIndex = partition(arr, start, end)
        quick_sort(arr, start, pivotIndex-1)
        quick_sort(arr, pivotIndex+1, end)


if __name__ == "__main__":
    arr = [5, 2, 1, 4, 3, 6]
    quick_sort(arr, 0, len(arr)-1)
    print(arr)
