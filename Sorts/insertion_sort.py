def insertion_sort(array):
    for i in range(1, len(array)):
        current_element = array[i]
        j = i - 1
        while j >= 0 and array[j] > current_element:
            array[j+1] = array[j]
            j -= 1
        array[j+1] = current_element
    return array


if __name__ == "__main__":
    array = [2, 5, 6, 3, 4, 1, 7]
    print(insertion_sort(array))
