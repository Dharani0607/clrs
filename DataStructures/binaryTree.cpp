#include<bits/stdc++.h>
#include<queue>
#include<stack>
#include<map>

using namespace std;

struct Node{
    int data;
    struct Node *left;
    struct Node *right;
};

class myBinaryTree{
    Node *root;

public:
    myBinaryTree(){
        root = NULL;
    }
    void insert(int value);
    Node *get_root();
    void pre_order(Node *root_pointer);
    void in_order(Node *root_pointer);
    void post_order(Node *root_pointer);
    void vertical_order();
    void DFT();
    void BFT();
};

void myBinaryTree::insert(int value){
    Node *newnode = new Node;
    newnode->data = value;
    newnode->left = NULL;
    newnode->right = NULL;
    if(root == NULL){
        root = newnode;
        return;
    }

    queue <Node*> q;
    q.push(root);
    while(!q.empty()){
        Node *current = q.front();
        q.pop();
        if(current->left == NULL){
            current->left = newnode;
            return;
        }
        else{
            q.push(current->left);
        }
        if(current->right == NULL){
            current->right = newnode;
            return;
        }
        else{
            q.push(current->right);
        }
    }
}

Node* myBinaryTree::get_root(){
    Node *root_pointer;
    root_pointer = root;
    return root_pointer;
}

void myBinaryTree::pre_order(Node *current){
    if(current == NULL)
        return;
    else{
        cout << current->data << " ";
        pre_order(current->left);
        pre_order(current->right);
    } 
}

void myBinaryTree::in_order(Node *current){
    if(current == NULL)
        return;
    else{
        in_order(current->left);
        cout << current->data << " ";
        in_order(current->right);
    }
}

void myBinaryTree::post_order(Node *current){
    if(current == NULL)
        return;
    else{
        post_order(current->left);
        post_order(current->right);
        cout << current->data << " ";
    }
}


void myBinaryTree::DFT(){
    stack <Node*> s;
    s.push(root);
    while(!s.empty()){
        Node *current = s.top();
        s.pop();
        cout << current->data << " ";
        if(current->right != NULL)
            s.push(current->right);
        if(current->left != NULL)
            s.push(current->left); 
    }
}


void myBinaryTree::BFT(){
    queue <Node*> q;
    q.push(root);
    while(!q.empty()){
       Node *current = q.front();
       q.pop();
       cout << current->data << " ";
       if(current->left != NULL)
           q.push(current->left);
       if(current->right != NULL)
           q.push(current->right);
    }
}

void myBinaryTree::vertical_order(){
    map <int,vector<int > > m;
    queue <Node*> q;
    queue <int> scores;
    q.push(root);
    scores.push(0);
    
    while(!q.empty()){
        Node *current = q.front();
        q.pop();
        int score = scores.front();
        scores.pop();
        m[score].push_back(current->data);
        if(current->left != NULL){
            q.push(current->left);
            scores.push(score-1);
        }
        if(current->right != NULL){
            q.push(current->right);
            scores.push(score+1);
        }
    }
    map <int,vector<int > > :: iterator itr;
    for(itr = m.begin(); itr != m.end(); itr++){
        for(int i=0; i < itr->second.size(); i++){
            cout << itr->second[i] << " ";
        }
        cout << "\n";
    }    
}

int main(){
    class myBinaryTree btree;
    btree.insert(10);
    btree.insert(12);
    btree.insert(14);
    btree.insert(26);
    btree.insert(34);
    btree.insert(17);
    btree.in_order(btree.get_root());
    cout << "\n";
    btree.insert(100);
    btree.post_order(btree.get_root());
    cout << "\n";
    btree.DFT();
    cout << "\n";
    btree.vertical_order();
    cout << "\n";
    return 0;
}
