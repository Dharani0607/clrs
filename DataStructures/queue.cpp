#include<bits/stdc++.h>
using namespace std;

struct Node{
    int data;
    struct Node *next;
};

class myQueue{
    Node *front;
    Node *rear;

public:
    myQueue(){
        front = NULL;
        rear = NULL;
    }
    void enqueue(int value);
    int dequeue();
};

void myQueue::enqueue(int value){
    Node *newNode = new Node;
    newNode->data = value;
    newNode->next = NULL;

    if(front == NULL){
        front = newNode;
        rear = newNode;
    }
    
    else{
        rear->next = newNode;
        rear = newNode;
    }
}

int myQueue::dequeue(){
    if(front == NULL){
        cout << "\nqueue is empty!" ;
        return -1000;
    }
    else{
        Node *current;
        current = front;
        front = front->next;
        int data = current->data;
        free(current);
        return data;
    }
}

int main()
{
    class myQueue q;
    q.enqueue(10);
    q.enqueue(20);
    q.enqueue(30);
    q.enqueue(40);
    cout << "\ndequeued: " << q.dequeue();
    cout << "\ndequeued: " << q.dequeue();
    cout << "\ndequeued: " << q.dequeue();
    cout << "\ndequeued: " << q.dequeue();
    cout << "\ndequeued: " << q.dequeue();    
    q.enqueue(300);
    q.enqueue(400);
    cout << "\ndequeued: " << q.dequeue();
    cout << "\ndequeued: " << q.dequeue();
    cout << "\ndequeued: " << q.dequeue();
}
