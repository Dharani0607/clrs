#include<bits/stdc++.h>
using namespace std;

struct Node{
    int data;
    struct Node *next;
};

class myStack{
    Node *top;
    int length;

public:
    myStack(){
        top = NULL;
        length = 0;
    }
 
    bool push(int value); 
    int pop(); 
    int peek(); 
    bool isEmpty(); 
    int size();
};

bool myStack::isEmpty(){
    if(top == NULL)
        return true;
    else
        return false;
}

bool myStack::push(int value){
    if(isEmpty()){
        top = new Node;
        top->data = value;
        top->next = NULL; 
    }
    else{
        Node *newNode = new Node;
        newNode->data = value;
        newNode->next = top;
        top = newNode;
    }
    length++;
}

int myStack::peek(){
    if(isEmpty()){
        cout << "Stack is Empty!! " ;
        return -10000;
    }
    else{
        return top->data;
    }
}

int myStack::pop(){
    if(isEmpty()){
        cout << "Stack is Empty!! " ;
        return -10000;
    }
    else{
        Node *current;
        current = top;
        top = top->next;
        int data = current->data;
        free(current);
        length--;
        return data;
    }
    
}

int myStack::size(){
    return length;
}

int main(){
    class myStack s;
    cout << "top: " << s.peek() << "\n";
    s.push(23);
    s.push(18);
    cout << "top: " << s.peek() << "\n";
    cout << "poped: " << s.pop() << "\n";
    cout << "top: " << s.peek() << "\n";
    cout << "size: " << s.size() << "\n";
    class myStack s1;
    cout << "top: " << s1.peek() << "\n";
    s1.push(231);
    s1.push(181);
    cout << "top: " << s1.peek() << "\n";
    cout << "poped: " << s1.pop() << "\n";
    cout << "top: " << s1.peek() << "\n";
    return 0;
}
