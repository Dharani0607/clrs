#include<bits/stdc++.h>
using namespace std;

struct Node{
    int data;
    struct Node *next;
};

class linked_list{
    Node *head;
    Node *tail;

public:
    linked_list(){
       head = NULL;
       tail = NULL;
    }
    void push_back(int value);
    void push_front(int value);
    int pop_front();
    void pop_back();
    void reverse();
    void show_list();
};

void linked_list::push_back(int value)
{
    Node *newnode = new Node;
    newnode->data = value;
    newnode->next = NULL;
    if(head == NULL){
        head = newnode;
        tail = newnode;
    }
    else{
        tail->next = newnode;
        tail = newnode;
    }
}

void linked_list::push_front(int value){
    Node *newnode = new Node;
    newnode->data = value;
    newnode->next = NULL;
    if(head == NULL){
        head = newnode;
        tail = newnode;
    }
    else{
        newnode->next = head;
        head = newnode;
    }
}

void linked_list::pop_back(){
    Node *itr;
    itr = head;
    if(itr->next == NULL){
        free(itr);
        head = NULL;
        tail = NULL;
    } 
    else{
        while(itr->next->next != NULL){
            itr = itr->next;
        }
    }
    tail = itr;
    Node *del;
    del = itr->next;
    itr->next = NULL;
    free(del);
}

void linked_list::show_list(){
    Node *itr;
    itr = head;
    if(itr != NULL){
        while(itr->next != NULL){
            cout << itr->data <<" ";
            itr = itr->next;
        }
        cout << itr->data << "\n";
    }
    else
        cout << "list is empty\n"; 
}

int main(){
    class linked_list list;
    list.push_back(3);
    //list.push_front(2);
    //list.push_front(1);
    //list.push_back(4);
    list.push_back(5);
    list.show_list();
    list.pop_back();
    list.show_list();
    return 0;
}






