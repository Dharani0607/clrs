#include<bits/stdc++.h>
#include<queue>
#include<vector>
using namespace std;

struct Node{
    int data;
    struct Node *left;
    struct Node *right;
};

class BinaryTree{
    Node *root;

public:
    BinaryTree(){
        root = NULL;
    }
    void insert(int value);
    void inorder(Node *btree);
    Node* get_root();
    bool find_path(Node *btree, int destination, vector<int> &path);
    int lca(vector<int> path1, vector<int> path2);
};

Node* BinaryTree::get_root(){
    return root;
}

void BinaryTree::insert(int value){
    Node *newnode = new Node;
    newnode->data = value;
    newnode->left = NULL;
    newnode->right = NULL;
    if(root == NULL){
        root = newnode;
        return;
    }
    else{
        queue <Node*> q;
        q.push(root);
        while(!q.empty()){
            Node *current = q.front();
            q.pop();
            if(current->left == NULL){
                current->left = newnode;
                break;
            }
            else{
                q.push(current->left);
            }
            if(current->right == NULL){
                current->right = newnode;
                break;
            }
            else{
                q.push(current->right);
            }
        }
    } 
}

void BinaryTree::inorder(Node *btree){
    if(btree == NULL)
        return;
    else{
        inorder(btree->left);
        cout << btree->data <<" ";
        inorder(btree->right);
    }
}

int BinaryTree::lca(vector<int> path1, vector<int> path2){
    int i;
    for(i = 0; i < path1.size() && i < path2.size(); i++){
        if(path1[i] != path2[i])
            break;
    }
    return path1[i-1];
}

bool BinaryTree::find_path(Node *btree, int destination, vector<int> &path){
    if(btree == NULL)
        return false;
    path.push_back(btree->data);
    if(btree->data == destination)
        return true;
    if((btree->left && find_path(btree->left, destination, path)) || 
       (btree->right && find_path(btree->right, destination, path)))
        return true;
    path.pop_back();
    return false;
}

int main(){
    class BinaryTree bt;
    bt.insert(7);
    bt.insert(5);
    bt.insert(9);
    bt.insert(4);
    bt.insert(6);
    bt.insert(17);
    bt.insert(15);
    bt.insert(19);
    bt.insert(14);
    bt.insert(16);
    vector<int> path1;
    vector<int> path2;
    bt.find_path(bt.get_root(), 14, path1);
    bt.find_path(bt.get_root(), 16, path2);
    vector<int> :: iterator itr;
    cout << "\n";
    for(itr = path1.begin(); itr != path1.end(); itr++){
        cout << *itr << " ";
    }
    cout << "\n";
    for(itr = path2.begin(); itr != path2.end(); itr++){
        cout << *itr << " ";
    }
    cout << "\n";
    cout << "Lowest common ancestor: " << bt.lca(path1, path2) << "\n";
    return 0;
}

